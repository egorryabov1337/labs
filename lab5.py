import math

def main():
    m = []
    try:
        command = int(input("G(1)  F(2)  Y(3)?: "))
        data = list(map(float, input('Введите а,x,х максимальное, количество шагов: ').split()))
        search = str(input('Число каких символов в строке хотим найти? '))
        a = int(data[0])
        x = int(data[1])
        xmax = int(data[2])
        number = int(data[3])
        size = (xmax-x)/number  # Вычисляем размерность шагов
    except ValueError:
        print("Ошибка")
        return 1


    step = 0  # Шаг

    for i in range(number):
        if command == 1:
            try:
                g = (7 * (10 * a ** 2 + 23 * a * x + 6 * x ** 2)) / ( 12 * a ** 2 - 43 * a * x + 10 * x **2)
                x += size
                step += 1
                print(step,"x=",x, "G = {0:.1f} ".format(g))
                m.append(g)

            except ZeroDivisionError:
                g = None
                x += size
                step += 1
                print(step, "x=", x, "G = {0:.1f} ".format(g))

        elif command == 2:
            try:
                f = math.sin(24 * a ** 2 + 29 * a * x + 7 * x **2)
                x += size
                step += 1
                print(step,"x=",x, "F = {0:.1f} " .format(f))
                m.append(f)

            except ValueError:
                f = None
                x += size
                step += 1
                print(step, "x=", x, "F = {0:.1f} " .format(f))

        elif command == 3:
            try:
                
                y = math.acos(-8 * a ** 2 + 2 * a * x + 3 * x **2 + 1 )
                x += size
                step += 1
                print(step,"x=",x, "Y = {0:.1f} " .format(y))
                m.append(y)

            except ValueError:
                y = None
                x += size
                step += 1
                print(step, "x=", x, "Y = {0:.1f} " .format(y))
        else:
            print("Команда не найдена")

    m = list(map(str, m))
    print("Результат - {0}".format(', '.join(m)))  # Выводим результат
    count = 0
    for i in range(len(m)):
        if m[i] == search:
            count += 1
    print("Число подходящих элементов - {0}".format(count))


while True or OverflowError:
    main()
    again = input("Хотите начать команду заново?(Y или N): ")
    if again not in ["Y", "y"]:
        break
