import math
import matplotlib.pyplot as plt


def main():
    x_lst = []
    y_lst = []
    try:
        command = int(input("Какую функциию выбрать? G(1) F(2) Y(3) "))
        a = float(input("Введите a: "))
        x = float(input("Введите x: "))
        xmax = float(input("Введите x максимальное: "))
        number = int(input("Количество шагов: "))
        size = (xmax - x)/number
    except ValueError:
        print("Ошибка")
        return 1

    step = 0

    for i in range(number):
        if command == 1:
            try:
                g = (7 * (10 * a ** 2 + 23 * a * x + 6 * x ** 2)) / ( 12 * a ** 2 - 43 * a * x + 10 * x **2)
                x += size
                step += 1
                print(step, "x=", x, "G = {0:.3f} ".format(g))
                x_lst.append(x)
                y_lst.append(g)

            except ZeroDivisionError:
                g = None
                x += size
                step += 1
                print(step, "x=", x, "G = {0:.3f} ".format(g))

        elif command == 2:
            try:
                f = math.sin(24 * a ** 2 + 29 * a * x + 7 * x **2)
                x += size
                step += 1
                print(step, "x=", x, "F = {0:.3f} ".format(f))
                x_lst.append(x)
                y_lst.append(f)

            except ValueError:
                f = None
                x += size
                step += 1
                print(step, "x=", x, "F = {0:.3f} ".format(f))

        elif command == 3:
            try:

                y = math.acos(-8 * a ** 2 + 2 * a * x + 3 * x **2 + 1 )
                x += size
                step += 1
                print(step, "x=", x, "Y = {0:.3f} ".format(y))
                x_lst.append(x)
                y_lst.append(y)

            except ValueError:
                y = None
                x += size
                step += 1
                print(step, "x=", x, "Y = {0:.3f} ".format(y))
        else:
            print("Не найдена команда")

    plt.xlabel('x --->')
    plt.ylabel('f(x) --->')
    plt.title('График функции')
    plt.plot(x_lst, y_lst, 'r.-')
    plt.show()
    fmax = max(y_lst)
    fmin = min(y_lst)
    print('f(x) minimum = ',fmin,'f(x) maximum = ',fmax)


while True or OverflowError:
    main()
    again = input("Хотите начать команду заново?(Y или N): ")
    if again not in ["Y", "y"]:
        break