def point_aff(r, x1, y1, x0, y0):  # Для вычисления диаметра
    d = r**2 - ((x1 - x0)**2) - ((y1-y0)**2)
    if d >= 0:
        return True
    else:
        return False


def counter_points(r, x, y, x0, y0):
    count = 0
    for x1, y1 in zip(x, y):
        if r ** 2 >= ((x1 - x0) ** 2 + (y1 - y0) ** 2):
            count += 1
    return count  # Количество посчитанных чисел
