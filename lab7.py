import math

gm = []
fm = []
ym = []

try:
    data = list(map(float, input('Enter а,x,х maximum, number of calculation steps: ').split()))
    a = int(data[0])
    x = int(data[1])
    xmax = int(data[2])
    number = int(data[3])
    size = (xmax - x) / number  # Вычисляем размерность шагов

except ValueError:
    print("The input is incorrect")

for i in range(number):
    try:
        g = (7 * (10 * a ** 2 + 23 * a * x + 6 * x ** 2))/( 12 * a ** 2 - 43 * a * x + 10 * x **2)
        gm.append(g)
        f = math.sin(24 * a ** 2 + 29 * a * x + 7 * x **2)
        fm.append(f)
        y = math.acos(-8 * a ** 2 + 2 * a * x + 3 * x **2 + 1 )
        ym.append(y)
    except ZeroDivisionError:
        g = None
        gm.append(g)
        f = None
        fm.append(f)
    except ValueError:
        y = None
        ym.append(y)
    x += size

file = open('lab7.txt', 'w') # Работаем с файлом, если его нет создаем.
file.write(f"{gm}\n{ym}\n{fm}")
file.close()

data = []

file = open('lab7.txt', 'r')   # Открываем файл для чтения
[data.append(line.split()) for line in file]  # На каждой линии свое значение
file.close()

for x in range(len(data[0])):  # Выводим значения
    print(f"G: f(x) = {data[0][x]}")
for x in range(len(data[1])):
    print(f"F: f(x) = {data[1][x]}")
for x in range(len(data[2])):
    print(f"Y: f(x) = {data[2][x]}")
