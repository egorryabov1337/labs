import math


rnd = True
result_dict = {'G': [], 'F': [], 'Y': []}

while rnd:

    try:
        a = float(input('Введите а:'))
        x_min = float(input('Введите минимальное значение x:'))
        x_max = float(input('Введите максимальное значение x:'))
        n = int(input('Введите количество шагов вычисления функции:'))
    except ValueError:
        print('Ошибка ввода')
        exit(1)

    x_lst = []  # Список x

    step = (x_max - x_min) / n

    for i in range(n):
        x = x_min + step * i
        x_lst.append(x)
        g1 = (7 * (10 * a ** 2 + 23 * a * x + 6 * x ** 2)) # числитель функции G
        g2 = ( 12 * a ** 2 - 43 * a * x + 10 * x **2)  # знаменатель функции G
        if math.isclose(g2, 0, abs_tol=0.0001):
            G = None
            result_dict['G'].append(G)
        else:
            G = g1 / g2
            result_dict['G'].append(G)

    for i in range(n):
        x = x_min + step * i
        d = (24 * a ** 2 + 29 * a * x + 7 * x **2)  # функция F
        if -1 <= math.sin(d) <= 1:
            F = math.sin(d)
            result_dict['F'].append(F)
        else:
            F = None
            result_dict['F'].append(F)

    for i in range(n):
        x = x_min + step * i
        m = (-8 * a ** 2 + 2 * a * x + 3 * x **2 + 1 )
        if -1 <= m <= 1:
            Y = math.acos(m)
            result_dict['Y'].append(Y)
        else:
            Y = None
            result_dict['Y'].append(Y)


    # Вывод значений аргумента и функций

    print(f'x: {", ".join([str(x) for x in x_lst])}')
    print(f'G: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in result_dict["G"]])}')
    print(f'F: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in result_dict["F"]])}')
    print(f'Y: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.5f}""") for y in result_dict["Y"]])}')

    # Продолжение или завершение цикла

    print('Если хотите продолжить нажмите 1, иначе - нажмите любую другую цифру')

    if int(input()) != 1:
        rnd = False
    else:
        x_lst = []
        result_dict["G"] = []
        result_dict["F"] = []
        result_dict["Y"] = []

