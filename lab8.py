import random
import func
import time


n = 100000  # Промежуток
stop = 1000000
step = 100000

try:
    r = float(input('Введите радиус = '))
except ValueError:
    print('Ошибка')
    exit()

file = open('time.txt', 'w')  # Работаем с файлом time.txt, если его нет, создаем

while n <= stop:  # Цикл
    x = [random.uniform(-50, 50) for point in range(n)]  # 10*5 создаем случайных чисел по оси x
    y = [random.uniform(-50, 50) for point in range(n)]  # По оси y
    x0, y0 = random.uniform(-50, 50), random.uniform(-50, 50)

    start = time.time()  # Время начала алгоритма
    print(f'Центр:{x0};{y0}  Радиус:{r} Количество точек:  {func.counter_points(r, x, y, x0, y0)}')
    finish = time.time()  # Время конца алгоритма
    amounttime = str(finish - start).replace('.', ',')  # Количество времени потраченного на алгоритм
    file.write(f'{amounttime}\n')  # Запись количества времени потраченного на алгоритм в файл

    n += step

file.close()
